"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_predaprey.py
```
"""

from pathlib import Path

import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.predaprey.solver import Simul

here = Path(__file__).absolute().parent
path_dir_save = here / "../fig/"
path_dir_save.mkdir(exist_ok=True)

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.1
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs + 1.5)
sim.state.state_phys.set_var("Y", sim.Ys + 5.5)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()
fig=plt.gcf()
fig.savefig(path_dir_save / "fig_simple_1.png")

# Note: if you want to modify the figure and/or save it, you can use
#ax = plt.gca()
#fig = ax.figure
#fig.savefig(path_fig / "fig_predaprey_XY.png")

sim.output.print_stdout.plot_XY_vs_time()
ax = plt.gca()
fig = ax.figure
fig.savefig(path_dir_save / "fig_predaprey_XY_vs_time_1.png")

#plt.show()
